## create cluster

```
$eksctl create cluster --name=scala-hello-world11 --nodes=1 --node-type t3.nano --region=us-east-2
2021-12-27 22:09:39 [ℹ]  eksctl version 0.77.0
2021-12-27 22:09:39 [ℹ]  using region us-east-2
2021-12-27 22:09:40 [ℹ]  setting availability zones to [us-east-2a us-east-2b us-east-2c]
2021-12-27 22:09:40 [ℹ]  subnets for us-east-2a - public:192.168.0.0/19 private:192.168.96.0/19
2021-12-27 22:09:40 [ℹ]  subnets for us-east-2b - public:192.168.32.0/19 private:192.168.128.0/19
2021-12-27 22:09:40 [ℹ]  subnets for us-east-2c - public:192.168.64.0/19 private:192.168.160.0/19
2021-12-27 22:09:40 [ℹ]  nodegroup "ng-ab4a5271" will use "" [AmazonLinux2/1.21]
2021-12-27 22:09:40 [ℹ]  using Kubernetes version 1.21
2021-12-27 22:09:40 [ℹ]  creating EKS cluster "scala-hello-world11" in "us-east-2" region with managed nodes
2021-12-27 22:09:40 [ℹ]  will create 2 separate CloudFormation stacks for cluster itself and the initial managed nodegroup
2021-12-27 22:09:40 [ℹ]  if you encounter any issues, check CloudFormation console or try 'eksctl utils describe-stacks --region=us-east-2 --cluster=scala-hello-world11'
2021-12-27 22:09:40 [ℹ]  CloudWatch logging will not be enabled for cluster "scala-hello-world11" in "us-east-2"
2021-12-27 22:09:40 [ℹ]  you can enable it with 'eksctl utils update-cluster-logging --enable-types={SPECIFY-YOUR-LOG-TYPES-HERE (e.g. all)} --region=us-east-2 --cluster=scala-hello-world11'
2021-12-27 22:09:40 [ℹ]  Kubernetes API endpoint access will use default of {publicAccess=true, privateAccess=false} for cluster "scala-hello-world11" in "us-east-2"
2021-12-27 22:09:40 [ℹ]
2 sequential tasks: { create cluster control plane "scala-hello-world11",
    2 sequential sub-tasks: {
        wait for control plane to become ready,
        create managed nodegroup "ng-ab4a5271",
    }
}
2021-12-27 22:09:40 [ℹ]  building cluster stack "eksctl-scala-hello-world11-cluster"
2021-12-27 22:09:41 [ℹ]  deploying stack "eksctl-scala-hello-world11-cluster"
2021-12-27 22:10:11 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-cluster"
2021-12-27 22:10:41 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-cluster"
2021-12-27 22:11:41 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-cluster"
2021-12-27 22:12:42 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-cluster"
2021-12-27 22:13:42 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-cluster"
2021-12-27 22:14:42 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-cluster"
2021-12-27 22:15:43 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-cluster"
2021-12-27 22:16:43 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-cluster"
2021-12-27 22:17:43 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-cluster"
2021-12-27 22:18:43 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-cluster"
2021-12-27 22:19:44 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-cluster"
2021-12-27 22:20:44 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-cluster"
2021-12-27 22:22:47 [ℹ]  building managed nodegroup stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:22:48 [ℹ]  deploying stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:22:48 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:23:04 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:23:21 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:23:41 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:23:58 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:24:18 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:24:38 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:24:57 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:25:14 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:25:32 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:25:49 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:26:05 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:26:23 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:26:40 [ℹ]  waiting for CloudFormation stack "eksctl-scala-hello-world11-nodegroup-ng-ab4a5271"
2021-12-27 22:26:40 [ℹ]  waiting for the control plane availability...
2021-12-27 22:26:40 [✔]  saved kubeconfig as "/Users/kevinmeredith/.kube/config"
2021-12-27 22:26:40 [ℹ]  no tasks
2021-12-27 22:26:40 [✔]  all EKS cluster resources for "scala-hello-world11" have been created
2021-12-27 22:26:40 [ℹ]  nodegroup "ng-ab4a5271" has 1 node(s)
2021-12-27 22:26:40 [ℹ]  node "ip-192-168-78-189.us-east-2.compute.internal" is ready
2021-12-27 22:26:40 [ℹ]  waiting for at least 1 node(s) to become ready in "ng-ab4a5271"
2021-12-27 22:26:40 [ℹ]  nodegroup "ng-ab4a5271" has 1 node(s)
2021-12-27 22:26:40 [ℹ]  node "ip-192-168-78-189.us-east-2.compute.internal" is ready
2021-12-27 22:26:42 [ℹ]  kubectl command should work with "/Users/kevinmeredith/.kube/config", try 'kubectl get nodes'
2021-12-27 22:26:42 [✔]  EKS cluster "scala-hello-world11" in "us-east-2" region is ready
$
```