ThisBuild / scalaVersion := "3.1.0"

libraryDependencies ++= Seq(
  "co.fs2" %% "fs2-core" % "3.2.4",
  "io.circe" %% "circe-core" % "0.14.1"
)