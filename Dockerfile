FROM openjdk:8
VOLUME /tmp
ADD /target/scala-3.1.0/ci-cd-test-assembly-0.1.0-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]